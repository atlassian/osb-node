/**
 * OSB Previous Values Object
 */
class PreviousValues {
  private serviceId: string;
  private planId: string;
  private organizationId: string;
  private spaceId: string;

  constructor ({service_id, plan_id, organization_id, space_id}:
                   {service_id: string, plan_id: string, organization_id: string, space_id: string}) {
    this.serviceId = service_id;
    this.planId = plan_id;
    this.organizationId = organization_id;
    this.spaceId = space_id;
  }
}

export = PreviousValues;
