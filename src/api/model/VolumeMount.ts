/**
 * OSB Volume Mount Object
 */
import Device = require('./Device');

class VolumeMount {
  public driver: string;
  public container_dir: string;
  // "r" or "rw"
  public mode: string;
  public device_type: string;
  public device: Device;

  public constructor({driver, container_dir, mode, device_type, device}) {
    this.driver = driver;
    this.container_dir = container_dir;
    this.mode = mode;
    this.device_type = device_type;
    if (device) {
      this.device = new Device(device);
    }
  }
}

export = VolumeMount;
