/**
 * OSB Deprovision Response
 */
class DeprovisionResponse {
    public operation?: string;

    public constructor({operation}: {operation?: string}) {
        if (operation) {
            this.operation = operation;
        }
    }
}

export = DeprovisionResponse;
