/**
 * OSB Catalog Object
 */
import Service = require('./Service');

class Catalog {
  public services: Service[];

  public constructor({services = []}) {
    this.services = services.map(s => new Service(s));
  }
}

export = Catalog;
