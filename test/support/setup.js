

process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.use(require('chai-as-promised'));
const sinon = require('sinon');

global.chai = chai;
global.should = chai.should();
global.expect = chai.expect;
global.sinon = sinon;
