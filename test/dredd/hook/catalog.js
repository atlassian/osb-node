

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');
const chai = require('chai');
const should = chai.should();
const after = hooks.after;

const testData = require('../test-data');

// after hooks - validation
after('catalog > /v2/catalog > Gets services registered within the broker > 200 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.CATALOG));
});
