

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');
const chai = require('chai');
const should = chai.should();
const before = hooks.before;
const after = hooks.after;

const testData = require('../test-data');

// --- before hooks ---
before('service_instances > /v2/service_instances/{instance_id}/last_operation > Gets the current state of the last operation upon the specified resource. > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

before('service_instances > /v2/service_instances/{instance_id}/last_operation > Gets the current state of the last operation upon the specified resource. > 410 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});


// --- after hooks - validation ---
// Last operation request
after('service_instances > /v2/service_instances/{instance_id}/last_operation > Gets the current state of the last operation upon the specified resource. > 200 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.LAST_OPERATION));
});

after('service_instances > /v2/service_instances/{instance_id}/last_operation > Gets the current state of the last operation upon the specified resource. > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BAD_REQUEST));
});

after('service_instances > /v2/service_instances/{instance_id}/last_operation > Gets the current state of the last operation upon the specified resource. > 410 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.EMPTY_RESPONSE));
});
