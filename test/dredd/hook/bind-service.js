

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');
const chai = require('chai');
const should = chai.should();
const before = hooks.before;
const after = hooks.after;

const testData = require('../test-data');

// --- before hooks ---
before('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

before('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 409 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

before('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

// --- after hooks - validation ---
after('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 200 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BIND_SERVICE_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 201 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BIND_SERVICE_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BAD_REQUEST));
});

after('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 409 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.EMPTY_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id}/service_bindings/{binding_id} > Binds to a service > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify({
    error: 'RequiresApp',
    description: 'This service supports generation of credentials through binding an application only.'
  }));
});
