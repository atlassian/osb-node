

/**
 * @file
 * App for api tests
 */

const express = require('express');

const OSB = require('../../dist/api/OSB');
const OSBService = require('../../dist/api/OSBService');
const testData = require('./test-data');

const BrokerError = require('../../dist/api/model/BrokerError');

const osbPath = '/v2';

const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const broker = new OSB(app, {osbPath: osbPath});

const SERVICE_INFO = testData.SERIVCE_INFO;
const BAD_REQUEST = testData.BAD_REQUEST;
const EMPTY_RESPONSE = testData.EMPTY_RESPONSE;
const ASYNC_REQUIRED_RESPONSE = testData.ASYNC_REQUIRED_RESPONSE;
const OPERATION_IN_PROGRESS_RESPONSE = testData.OPERATION_IN_PROGRESS_RESPONSE;
const LAST_OPERATION = testData.LAST_OPERATION;
const PROVISION_RESPONSE = testData.PROVISION_RESPONSE;
const BIND_SERVICE_RESPONSE = testData.BIND_SERVICE_RESPONSE;

broker.registerService(new OSBService({
  getServiceInfo,
  provisionServiceInstance,
  updateServiceInstance,
  deprovisionServiceInstance,
  bindService,
  unbindService,
  pollLastOperation
}));

function getServiceInfo () {
  return SERVICE_INFO;
}

function provisionServiceInstance (request) {
  const expectedStatusCode = getExpectedStatusCode(request);

  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
  case '201':
    result = PROVISION_RESPONSE;
    break;
  case '202':
    result = OPERATION_IN_PROGRESS_RESPONSE;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '409':
    result = EMPTY_RESPONSE;
    break;
  case '422':
    result = new BrokerError({description: 'broker only supports asynchronous provisioning for the requested plan'});
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };

}

function updateServiceInstance (request) {
  const expectedStatusCode = getExpectedStatusCode(request);
  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
    result = EMPTY_RESPONSE;
    break;
  case '202':
    result = OPERATION_IN_PROGRESS_RESPONSE;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '422':
    result = new BrokerError({description: 'broker only supports asynchronous provisioning for the requested plan'});
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };
}

function deprovisionServiceInstance (request) {
  const expectedStatusCode = getExpectedStatusCode(request);
  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
    result = EMPTY_RESPONSE;
    break;
  case '202':
    result = OPERATION_IN_PROGRESS_RESPONSE ;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '410':
    result = EMPTY_RESPONSE;
    break;
  case '422':
    result = ASYNC_REQUIRED_RESPONSE;
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };
}

function bindService (request) {
  const expectedStatusCode = getExpectedStatusCode(request);
  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
  case '201':
    result = BIND_SERVICE_RESPONSE;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '409':
    result = EMPTY_RESPONSE;
    break;
  case '422':
    result = new BrokerError({
      error: 'RequiresApp',
      description: 'This service supports generation of credentials through binding an application only.'
    });
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };
}

function unbindService (request) {
  const expectedStatusCode = getExpectedStatusCode(request);
  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
    result = EMPTY_RESPONSE;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '410':
    result = EMPTY_RESPONSE;
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };
}

function pollLastOperation (request) {
  const expectedStatusCode = getExpectedStatusCode(request);
  let result = '';
  let status = expectedStatusCode;

  switch (expectedStatusCode) {
  case '200':
    result = LAST_OPERATION;
    break;
  case '400':
    result = BAD_REQUEST;
    break;
  case '410':
    result = EMPTY_RESPONSE;
    break;
  default:
    status = '500';
  }

  return {
    statusCode: status,
    data: result
  };
}

broker.start();
app.listen(3000);

function getExpectedStatusCode (request) {
  return request.instanceId;
}
