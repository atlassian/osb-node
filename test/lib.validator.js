'use strict';

const serviceSchema = require('../src/schema/service.json');
const Validator = require('../dist/lib/validator');
const Service = require('../dist/api/model/Service');
const Plan = require('../dist/api/model/Plan');

describe('Validator', function () {
  let validator;

  describe('#Service', function () {
    let service;

    beforeEach(function () {
      validator = new Validator('service', serviceSchema);
      service = new Service({
        name: 'service',
        id: 'service-id',
        description: 'service',
        bindable: true,
        plans: [new Plan({
          id: 'plan-id',
          name: 'plan',
          description: 'plan'
        })]
      });
    });

    it('Fail when missing service\'s required properties', function () {
      service = new Service({});
      validator.validate(service).should.deep.equals([
        'service must have required property \'id\'',
        'service must have required property \'name\'',
        'service must have required property \'description\'',
        'service must have required property \'bindable\'',
        'service must have required property \'plans\''
      ]);
    });

    it('Fail when having extra property', function () {
      service.x = 'x';
      validator.validate(service).should.deep.equals(['service must NOT have additional properties: x']);
    });

    it('Fail when missing plans is empty', function () {
      service.plans = [];
      validator.validate(service).should.deep.equals(['plans must NOT have fewer than 1 items']);
    });

    it('Fail when missing plan\'s required properties', function () {
      service.plans = [new Plan({})];
      validator.validate(service).should.deep.equals([
        'plans/0 must have required property \'id\'',
        'plans/0 must have required property \'name\'',
        'plans/0 must have required property \'description\''
      ]);
    });
  });
});
