# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.3] - 2022-11-23
- Per OSB spec, the operaiton included in the DeprovisionResponse payload is not required and should be returned for
  a 202 (in progress) response, but not where the service instance was immediately deleted (200 response code).

## [0.1.2] - 2022-05-26

### Changed
- Adjusted our schema validators to use lower case `description` field as well as remove deprecated constructor options
  on validator to remove log warnings.

## [0.1.1] - 2022-03-30

### Changed
- Upgrade of avj to latest (8.10.0) due to a security vulnerability in 5.3.0
